#include "tortuga.h"
#include <QPoint>
#include <QtMath>
#include <math.h>

Tortuga::Tortuga()
{
    posicion_actual = new QPointF(0,0);
    angle = 0;
}

Tortuga::Tortuga(int x, int y)
{
    posicion_actual = new QPointF(x,y);
    angle = 0;
}

void Tortuga::setAngle(int a)
{
    float fradians = qDegreesToRadians((float)a);
    angle += fradians;
}
