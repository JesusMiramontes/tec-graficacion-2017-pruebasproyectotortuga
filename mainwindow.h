#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "tortuga.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    QPixmap *pix;
    QPainter* paint;

    QPixmap *pixApuntador;
    QPainter* paintApuntador;

    Tortuga *t;
    ~MainWindow();

private slots:
    void on_pushButton_clicked();
    void dibujarLinea(int x1, int y1, int x2, int y2, int width=3);
    void dibujarLinea(QPoint *p1, QPoint *p2, int width=3);
    void dibujarLinea(QPointF *p1, QPointF *p2, int width=3);
    void dibujarTriangulo(QPoint *p1, QPoint *p2, QPoint *p3);
    void dibujarLinea(int length=3);
    void dibujarLineaWithAngle(int angle,int length=3);
    void on_btnRotar_clicked();

    void on_pushButton_2_clicked();

    void on_horizontalSlider_valueChanged(int value);

    void on_Performcmd_clicked();

    void update_triangle();
    void dibujarLineaSobreApuntador(QPointF *p1, QPointF *p2);
    QPointF *rotar(int angle, int length=3);
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
