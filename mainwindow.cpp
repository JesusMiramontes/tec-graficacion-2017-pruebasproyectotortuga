#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPixmap>
#include <QPainter>
#include <QBrush>
#include <QPalette>
#include <QImage>
#include <QPoint>
#include "tortuga.h"
#include <QtMath>
#include <math.h>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    pix = new QPixmap(ui->label->width(), ui->label->height()); //Tamaño canvas
    pix->fill(Qt::transparent); //Fondo transparente
    paint = new QPainter(pix);
    t = new Tortuga(ui->label->width()/2, ui->label->height()/2);
    dibujarLinea(t->posicion_actual,t->posicion_actual);

    ui->oStartX->setText(QString::number(t->posicion_actual->x()));
    ui->oStartY->setText(QString::number(t->posicion_actual->y()));

    /*int tamano_lado = 10;
    QPoint *pt2 = new QPoint(t->posicion_actual->x()-(tamano_lado/2),t->posicion_actual->y()+tamano_lado);
    QPoint *pt3 = new QPoint(t->posicion_actual->x()+(tamano_lado/2),t->posicion_actual->y()+tamano_lado);
    dibujarTriangulo(t->posicion_actual,pt2,pt3);*/

    update_triangle();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    /*dibujarLinea(ui->ix1->text().toInt(),
                 ui->iy1->text().toInt(),
                 ui->ix2->text().toInt(),
                 ui->iy2->text().toInt(),
                 ui->iwidth->text().toInt());*/
    QPointF *p2 = new QPointF(ui->ix2->text().toInt(),
                           ui->iy2->text().toInt());
    dibujarLinea(t->posicion_actual,p2);
    t->posicion_actual = p2;
    ui->pointer->setGeometry(t->posicion_actual->x()-1,t->posicion_actual->y()-9,16,16);
}

void MainWindow::dibujarLinea(int x1, int y1, int x2, int y2, int width)
{
    QPen pen;  // creates a default pen

    pen.setStyle(Qt::SolidLine); //Estilo de linea
    pen.setWidth(width); //Ancho de linea
    pen.setBrush(Qt::black); //Color de lina
    pen.setCapStyle(Qt::SquareCap); //Forma de extremos de lina (cuadrado, redondeado, etc)
    //pen.setJoinStyle(Qt::RoundJoin);

    paint->setPen(pen); //Color separador
    paint->drawLine(x1, y1, x2, y2);

    ui->label->setPixmap(*pix);
}

void MainWindow::dibujarLinea(QPoint *p1, QPoint *p2, int width)
{
    QPen pen;  // creates a default pen

    pen.setStyle(Qt::SolidLine); //Estilo de linea
    pen.setWidth(width); //Ancho de linea
    pen.setBrush(Qt::black); //Color de lina
    pen.setCapStyle(Qt::SquareCap); //Forma de extremos de lina (cuadrado, redondeado, etc)
    //pen.setJoinStyle(Qt::RoundJoin);

    paint->setPen(pen); //Color separador
    paint->drawLine(*p1, *p2);

    ui->label->setPixmap(*pix);
}

void MainWindow::dibujarLinea(QPointF *p1, QPointF *p2, int width)
{
    QPen pen;  // creates a default pen

    pen.setStyle(Qt::SolidLine); //Estilo de linea
    pen.setWidth(width); //Ancho de linea
    pen.setBrush(Qt::black); //Color de lina
    pen.setCapStyle(Qt::SquareCap); //Forma de extremos de lina (cuadrado, redondeado, etc)
    //pen.setJoinStyle(Qt::RoundJoin);

    paint->setPen(pen); //Color separador
    paint->drawLine(*p1, *p2);

    ui->label->setPixmap(*pix);
}

void MainWindow::dibujarTriangulo(QPoint *p1, QPoint *p2, QPoint *p3)
{
    dibujarLinea(p1,p2);
    dibujarLinea(p2,p3);
    dibujarLinea(p3,p1);
}

void MainWindow::dibujarLinea(int length)
{
    qreal new_x, new_y;

    float fradians = t->angle;

    qreal radians = (qreal)fradians;
    new_x = (qreal)(t->posicion_actual->x() + length * qCos(radians));
    new_y = (qreal)(t->posicion_actual->y() + length * qSin(radians));

    QPointF* new_point = new QPointF(new_x, new_y);
    dibujarLinea(t->posicion_actual,new_point);
    t->posicion_actual = new_point;
    ui->pointer->setGeometry(t->posicion_actual->x()-1,t->posicion_actual->y()-9,16,16);
}

void MainWindow::dibujarLineaWithAngle(int angle, int length)
{
    qreal new_x, new_y;

    float fradians = qDegreesToRadians((float)angle);

    qreal radians = (qreal)fradians;
    new_x = (qreal)(t->posicion_actual->x() + length * qCos(radians));
    new_y = (qreal)(t->posicion_actual->y() + length * qSin(radians));

    QPointF* new_point = new QPointF(new_x, new_y);
    dibujarLinea(t->posicion_actual,new_point);
    //t->posicion_actual = new_point;
    //ui->pointer->setGeometry(t->posicion_actual->x()-1,t->posicion_actual->y()-9,16,16);
}



void MainWindow::on_btnRotar_clicked()
{
    ////int new_x = t->posicion_actual->x() + iLength * Math.Cos(theta)
    qreal new_x, new_y;
    ////new_x = t->posicion_actual->x() + ui->iLength->text().toInt() * qCos(ui->iTheta->text().toInt());
    ////new_y = t->posicion_actual->y() + ui->iLength->text().toInt() * qSin(ui->iTheta->text().toInt());

    //float fradians = qDegreesToRadians(ui->iTheta->text().toFloat());
    float fradians = qDegreesToRadians(ui->iTheta->text().toFloat());
    //fradians = fradians * (3.141516 / 180);
    qreal radians = (qreal)fradians;//round(fradians);
    new_x = (qreal)(t->posicion_actual->x() + ui->iLength->text().toInt() * qCos(radians));
    new_y = (qreal)(t->posicion_actual->y() + ui->iLength->text().toInt() * qSin(radians));

    QPointF* new_point = new QPointF(new_x, new_y);

    dibujarLinea(t->posicion_actual,new_point);

    ui->oCos->setText(QString::number( qCos(radians) ));
    ui->oSin->setText(QString::number(qSin(radians)));
    ui->oNewX->setText(QString::number(new_x));
    ui->oNewY->setText(QString::number(new_y));



}

void MainWindow::on_pushButton_2_clicked()
{
    //ui->setupUi(this);
    pix = new QPixmap(ui->label->width(), ui->label->height()); //Tamaño canvas
    pix->fill(Qt::transparent); //Fondo transparente
    paint = new QPainter(pix);
    t = new Tortuga(ui->label->width()/2, ui->label->height()/2);
    dibujarLinea(t->posicion_actual,t->posicion_actual);

    ui->oStartX->setText(QString::number(t->posicion_actual->x()));
    ui->oStartY->setText(QString::number(t->posicion_actual->y()));
}

void MainWindow::on_horizontalSlider_valueChanged(int value)
{
    ui->iTheta->setText(QString::number( value));
    qreal new_x, new_y;
    ////new_x = t->posicion_actual->x() + ui->iLength->text().toInt() * qCos(ui->iTheta->text().toInt());
    ////new_y = t->posicion_actual->y() + ui->iLength->text().toInt() * qSin(ui->iTheta->text().toInt());

    //float fradians = qDegreesToRadians(ui->iTheta->text().toFloat());
    float fradians = qDegreesToRadians((float)value);
    //fradians = fradians * (3.141516 / 180);
    qreal radians = (qreal)fradians;//round(fradians);
    new_x = (qreal)(t->posicion_actual->x() + ui->iLength->text().toInt() * qCos(radians));
    new_y = (qreal)(t->posicion_actual->y() + ui->iLength->text().toInt() * qSin(radians));

    QPointF* new_point = new QPointF(new_x, new_y);

    dibujarLinea(t->posicion_actual,new_point);

    ui->oCos->setText(QString::number( qCos(radians) ));
    ui->oSin->setText(QString::number(qSin(radians)));
    ui->oNewX->setText(QString::number(new_x));
    ui->oNewY->setText(QString::number(new_y));
}

void MainWindow::on_Performcmd_clicked()
{
    QStringList array = ui->iLinecmd->text().split(' ');
    for (int i = 0; i < array.size(); i++){
        if (array.at(i) == "rt"){
            QString s = array.at(i+1);
            t->setAngle(s.toInt());
        } else if (array.at(i) == "lt"){
            QString s = array.at(i+1);
            t->setAngle(s.toInt() * - 1);
        } else if (array.at(i) == "fd"){
            QString s = array.at(i+1);
            dibujarLinea(s.toInt());
        } else if (array.at(i) == "bd"){
            QMessageBox msgBox;
            msgBox.setText("bd found.");
            msgBox.exec();
        }
    }
    update_triangle();
}

void MainWindow::update_triangle()
{
    pixApuntador = new QPixmap(50, 50); //Tamaño canvas
    pixApuntador->fill(Qt::transparent); //Fondo transparente
    paintApuntador = new QPainter(pixApuntador);
    QPointF* wingA = rotar(t->angle+135,10);
    QPointF* wingB = rotar(t->angle-135,10);
    //dibujarLineaWithAngle(t->angle+135,10);
    //dibujarLineaWithAngle(t->angle-135,10);
    //dibujarLinea(t->posicion_actual,wingA);
    //dibujarLinea(t->posicion_actual,wingB);
    ui->pointer->setGeometry(t->posicion_actual->x()-1,t->posicion_actual->y()-8,10,10);
    //dibujarLineaSobreApuntador(wingA,wingB);
}

void MainWindow::dibujarLineaSobreApuntador(QPointF *p1, QPointF *p2)
{
    QPen pen;  // creates a default pen

    pen.setStyle(Qt::SolidLine); //Estilo de linea
    pen.setWidth(3); //Ancho de linea
    pen.setBrush(Qt::black); //Color de lina
    pen.setCapStyle(Qt::SquareCap); //Forma de extremos de lina (cuadrado, redondeado, etc)
    //pen.setJoinStyle(Qt::RoundJoin);

    paint->setPen(pen); //Color separador
    paint->drawLine(*t->posicion_actual, *p1);
    paint->drawLine(*t->posicion_actual, *p2);

    ui->pointer->setPixmap(*pixApuntador);
}

QPointF* MainWindow::rotar(int angle, int length)
{
    qreal new_x, new_y;

    float fradians = qDegreesToRadians((float)angle);

    qreal radians = (qreal)fradians;
    new_x = (qreal)(t->posicion_actual->x() + length * qCos(radians));
    new_y = (qreal)(t->posicion_actual->y() + length * qSin(radians));

    QPointF* new_point = new QPointF(new_x, new_y);
    return new_point;
}
